package innovation;

import java.util.HashSet;
import java.util.Set;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.NumericDocValuesField;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexableField;

public class DocumentBuilder {
	private Set<IndexableField> fields = new HashSet<>();

	public DocumentBuilder indexLong(final String fieldName, final long value) {
		fields.add(new NumericDocValuesField(fieldName, value));
		return this;
	}

	public DocumentBuilder indexString(final String fieldName, final String value) {
		fields.add(new StringField(fieldName, value, Store.NO));
		return this;
	}

	public DocumentBuilder storeLong(final String fieldName, final long value) {
		indexLong(fieldName, value);
		fields.add(new StoredField(fieldName, value));
		return this;
	}

	public DocumentBuilder storeString(final String fieldName, final String value) {
		fields.add(new StringField(fieldName, value, Store.YES));
		return this;
	}

	public Document build() {
		final Document document = new Document();
		for (final IndexableField field : fields) {
			document.add(field);
		}
		return document;
	}
}
