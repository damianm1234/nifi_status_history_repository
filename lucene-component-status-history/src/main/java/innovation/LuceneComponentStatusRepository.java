package innovation;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.NumericDocValuesField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.SortField.Type;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.nifi.controller.status.ConnectionStatus;
import org.apache.nifi.controller.status.ProcessGroupStatus;
import org.apache.nifi.controller.status.ProcessorStatus;
import org.apache.nifi.controller.status.RemoteProcessGroupStatus;
import org.apache.nifi.controller.status.history.ComponentStatusRepository;
import org.apache.nifi.controller.status.history.ConnectionStatusDescriptor;
import org.apache.nifi.controller.status.history.CounterMetricDescriptor;
import org.apache.nifi.controller.status.history.GarbageCollectionHistory;
import org.apache.nifi.controller.status.history.GarbageCollectionStatus;
import org.apache.nifi.controller.status.history.MetricDescriptor;
import org.apache.nifi.controller.status.history.ProcessGroupStatusDescriptor;
import org.apache.nifi.controller.status.history.ProcessorStatusDescriptor;
import org.apache.nifi.controller.status.history.RemoteProcessGroupStatusDescriptor;
import org.apache.nifi.controller.status.history.StandardGarbageCollectionHistory;
import org.apache.nifi.controller.status.history.StandardGarbageCollectionStatus;
import org.apache.nifi.controller.status.history.StandardStatusHistory;
import org.apache.nifi.controller.status.history.StandardStatusSnapshot;
import org.apache.nifi.controller.status.history.StatusHistory;
import org.apache.nifi.controller.status.history.StatusSnapshot;
import org.apache.nifi.util.NiFiProperties;

/**
 * LuceneComponentStatusRepository is an implementation of
 * {@link ComponentStatusRepository} that uses Apache Lucene to index the stats
 * to the file system.
 */
public class LuceneComponentStatusRepository implements ComponentStatusRepository {

	/**
	 * The NiFi properties file property to specify the file system path for the
	 * Lucene index.
	 */
	public static final String INDEX_PATH = "innovation.components.status.repository.path";

	/**
	 * The default index path to use if none is specified.
	 */
	public static final String DEFAULT_INDEX_PATH = "./status_history_repository";

	/**
	 * The name of the field that states the type of document.
	 */
	private static final String DOCUMENT_TYPE = "Document Type";

	/**
	 * The document type value for a garbage collector snapshot.
	 */
	private static final String GC_SNAPSHOT_DOC = "GC Snapshot Doc";

	/**
	 * The document type value for a component status snapshot.
	 */
	private static final String COMPONENT_SNAPSHOT_DOC = "Component Snapshot Doc";

	private static final String TIMESTAMP = "Timestamp";
	private static final String MEMORY_MANAGER_NAME = "Memory Manager Name";
	private static final String COLLECTION_COUNT = "Collection Count";
	private static final String COLLECTION_MILLIS = "Collection Millis";
	private static final String COUNTER_PREFIX = "counter.";

	private static final Set<MetricDescriptor<?>> DEFAULT_PROCESSOR_METRICS = Arrays
			.stream(ProcessorStatusDescriptor.values()).map(ProcessorStatusDescriptor::getDescriptor)
			.collect(Collectors.toSet());
	private static final Set<MetricDescriptor<?>> DEFAULT_CONNECTION_METRICS = Arrays
			.stream(ConnectionStatusDescriptor.values()).map(ConnectionStatusDescriptor::getDescriptor)
			.collect(Collectors.toSet());
	private static final Set<MetricDescriptor<?>> DEFAULT_GROUP_METRICS = Arrays
			.stream(ProcessGroupStatusDescriptor.values()).map(ProcessGroupStatusDescriptor::getDescriptor)
			.collect(Collectors.toSet());
	private static final Set<MetricDescriptor<?>> DEFAULT_RPG_METRICS = Arrays
			.stream(RemoteProcessGroupStatusDescriptor.values()).map(RemoteProcessGroupStatusDescriptor::getDescriptor)
			.collect(Collectors.toSet());

	private final StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
	private final FSDirectory directory;
	private final IndexWriter indexWriter;
	private DirectoryReader directoryReader = null;
	private IndexSearcher indexSearcher = null;

	/**
	 * Default constructor to support service loading.
	 */
	public LuceneComponentStatusRepository() {
		// Used only for service loading.
		directory = null;
		indexWriter = null;
	}

	/**
	 * Constructs a new instance of LuceneComponentStatusRepository.
	 * 
	 * @param nifiProperties The NiFi properties with which to configure the
	 *                       component status repository.
	 */
	public LuceneComponentStatusRepository(final NiFiProperties nifiProperties) {
		// Find where we're putting our index.
		final String statusHistoryRepositoryPathProperty = nifiProperties.getProperty(INDEX_PATH, DEFAULT_INDEX_PATH);
		try {
			// Open a file system directory for Lucene to use to read and write the index.
			directory = FSDirectory.open(Paths.get(statusHistoryRepositoryPathProperty));
		} catch (final IOException e) {
			throw new IllegalArgumentException(
					"Could not create filesystem directory with path: " + statusHistoryRepositoryPathProperty, e);
		}

		try {
			// Create the index writer.
			final IndexWriterConfig indexWriterConfig = new IndexWriterConfig(standardAnalyzer);
			indexWriterConfig.setOpenMode(OpenMode.CREATE_OR_APPEND);
			indexWriter = new IndexWriter(directory, indexWriterConfig);
			// If the index is new, we need to commit to create a segment file to avoid
			// problems creating the index reader.
			indexWriter.commit();
		} catch (final IOException e) {
			throw new IllegalStateException("Could not initialise index writer", e);
		}
	}

	/**
	 * Returns a reference to the index searcher. If the index is changed after an
	 * index reader has been created then it will not see the changes to the index.
	 * Lucene provides a mechanism to efficiently recreate an index reader if the
	 * index has changed.
	 * 
	 * @return an index searcher.
	 * @throws IOException if there is a problem obtaining the index searcher.
	 */
	private IndexSearcher getIndexSearcher() throws IOException {
		if (directoryReader == null) {
			directoryReader = DirectoryReader.open(directory);
			indexSearcher = new IndexSearcher(directoryReader);
		} else {
			final DirectoryReader newDirectoryReader = DirectoryReader.openIfChanged(directoryReader);
			if (newDirectoryReader != null) {
				directoryReader = newDirectoryReader;
				indexSearcher = new IndexSearcher(directoryReader);
			}
		}

		return indexSearcher;
	}

	@Override
	public void capture(final ProcessGroupStatus rootGroupStatus,
			final List<GarbageCollectionStatus> garbageCollectionStatuses) {
		capture(rootGroupStatus, garbageCollectionStatuses, new Date());
	}

	@Override
	public void capture(final ProcessGroupStatus rootGroupStatus,
			final List<GarbageCollectionStatus> garbageCollectionStatuses, final Date timestamp) {
		try {
			indexProcessGroupStatus(indexWriter, rootGroupStatus, timestamp);
			indexGarbageCollectionStatus(indexWriter, garbageCollectionStatuses);
			indexWriter.commit();
		} catch (final IOException e) {
			throw new RuntimeException("Could not capture process group status: " + rootGroupStatus, e);
		}
	}

	private void indexProcessGroupStatus(final IndexWriter indexWriter, final ProcessGroupStatus processGroupStatus,
			final Date timestamp) throws IOException {
		// Capture status for the process group
		indexWriter.addDocument(createStatusSnapshotDocument(processGroupStatus, timestamp));

		// Capture statuses for the Processors
		for (final ProcessorStatus processorStatus : processGroupStatus.getProcessorStatus()) {
			indexWriter.addDocument(createStatusSnapshotDocument(processorStatus, timestamp));
		}

		// Capture statuses for the Connections
		for (final ConnectionStatus connectionStatus : processGroupStatus.getConnectionStatus()) {
			indexWriter.addDocument(createStatusSnapshotDocument(connectionStatus, timestamp));
		}

		// Capture statuses for the RPG's
		for (final RemoteProcessGroupStatus remoteProcessGroupStatus : processGroupStatus
				.getRemoteProcessGroupStatus()) {
			indexWriter.addDocument(createStatusSnapshotDocument(remoteProcessGroupStatus, timestamp));
		}

		// Capture statuses for the child groups
		for (final ProcessGroupStatus childStatus : processGroupStatus.getProcessGroupStatus()) {
			indexProcessGroupStatus(indexWriter, childStatus, timestamp);
		}
	}

	private void indexGarbageCollectionStatus(final IndexWriter indexWriter,
			final List<GarbageCollectionStatus> garbageCollectionStatuses) throws IOException {
		for (final GarbageCollectionStatus garbageCollectionStatus : garbageCollectionStatuses) {
			final Document document = new DocumentBuilder().storeString(DOCUMENT_TYPE, GC_SNAPSHOT_DOC)
					.storeString(MEMORY_MANAGER_NAME, garbageCollectionStatus.getMemoryManagerName())
					.storeLong(COLLECTION_COUNT, garbageCollectionStatus.getCollectionCount())
					.storeLong(COLLECTION_MILLIS, garbageCollectionStatus.getCollectionMillis())
					.storeLong(TIMESTAMP, garbageCollectionStatus.getTimestamp().getTime()).build();
			indexWriter.addDocument(document);
		}
	}

	@Override
	public Date getLastCaptureDate() {
		try {
			final IndexSearcher indexSearcher = getIndexSearcher();

			final MatchAllDocsQuery matchAllDocsQuery = new MatchAllDocsQuery();
			final Sort sort = new Sort(new SortField(TIMESTAMP, SortField.Type.LONG, true));
			final TopFieldDocs topFieldDocs = indexSearcher.search(matchAllDocsQuery, 1, sort);
			final ScoreDoc scoreDoc = topFieldDocs.scoreDocs[0];
			final Document document = indexSearcher.doc(scoreDoc.doc);
			return new Date(Long.parseLong(document.get(TIMESTAMP)));
		} catch (final IOException e) {
			throw new RuntimeException("Could not find last capture date", e);
		}
	}

	private StatusHistory getStatusHistory(final String componentId, final Date start, final Date end,
			final int preferredDataPoints, final Function<Document, StatusSnapshot> converter) throws IOException {
		// Get an index searcher for the up-to-date index
		final IndexSearcher indexSearcher = getIndexSearcher();

		// A null start should be considered to be the start of time.
		final long startLong = start != null ? start.getTime() : Long.MIN_VALUE;

		// A null end should be considered to be the current time.
		final long endLong = end != null ? end.getTime() : System.currentTimeMillis();

		// Find documents for the component ID within the specified time range, sorted
		// by timestamp.
		final BooleanQuery query = new BooleanQuery.Builder()
				.add(new BooleanClause(new TermQuery(new Term(DOCUMENT_TYPE, COMPONENT_SNAPSHOT_DOC)), Occur.MUST))
				.add(new BooleanClause(new TermQuery(new Term(COMPONENT_DETAIL_ID, componentId)), Occur.MUST))
				.add(new BooleanClause(NumericDocValuesField.newSlowRangeQuery(TIMESTAMP, startLong, endLong),
						Occur.MUST))
				.build();
		final TopDocs topDocs = indexSearcher.search(query, preferredDataPoints,
				new Sort(new SortField(TIMESTAMP, Type.LONG)));

		// We need to turn the found documents into component details, and snapshots.
		final Map<String, String> componentDetails;
		final List<StatusSnapshot> snapshots;

		if (topDocs.totalHits.value > 0) {
			// Turn each document into a NiFi snapshot
			snapshots = new ArrayList<>((int) topDocs.totalHits.value);
			final ListIterator<ScoreDoc> iterator = Arrays.asList(topDocs.scoreDocs).listIterator();
			Document document;
			do {
				final ScoreDoc scoreDoc = iterator.next();
				document = directoryReader.document(scoreDoc.doc);

				// Use the supplied converter to turn the document into the appropriate
				// snapshot.
				snapshots.add(converter.apply(document));
			} while (iterator.hasNext());

			// We use the last, most recent, document to populate the component details.
			componentDetails = Map.of(COMPONENT_DETAIL_ID, document.get(COMPONENT_DETAIL_ID), COMPONENT_DETAIL_NAME,
					document.get(COMPONENT_DETAIL_NAME));
		} else {
			// If there were no results, we use empty collections.
			componentDetails = Collections.emptyMap();
			snapshots = Collections.emptyList();
		}

		return new StandardStatusHistory(snapshots, componentDetails, new Date());
	}

	@Override
	public StatusHistory getConnectionStatusHistory(final String connectionId, final Date start, final Date end,
			final int preferredDataPoints) {
		try {
			return getStatusHistory(connectionId, start, end, preferredDataPoints, (document) -> {
				final StandardStatusSnapshot snapshot = new StandardStatusSnapshot(DEFAULT_CONNECTION_METRICS);
				snapshot.setTimestamp(new Date(Long.parseLong(document.get(TIMESTAMP))));

				for (ConnectionStatusDescriptor descriptor : ConnectionStatusDescriptor.values()) {
					final String documentField = document.get(descriptor.getField());
					snapshot.addStatusMetric(descriptor.getDescriptor(), Long.parseLong(documentField));
				}
				return snapshot;
			});
		} catch (final IOException e) {
			throw new RuntimeException("Could not get status history for connection: " + connectionId, e);
		}
	}

	@Override
	public StatusHistory getProcessGroupStatusHistory(final String processGroupId, final Date start, final Date end,
			final int preferredDataPoints) {
		try {
			return getStatusHistory(processGroupId, start, end, preferredDataPoints, (document) -> {
				final StandardStatusSnapshot snapshot = new StandardStatusSnapshot(DEFAULT_GROUP_METRICS);
				snapshot.setTimestamp(new Date(Long.parseLong(document.get(TIMESTAMP))));

				for (ProcessGroupStatusDescriptor descriptor : ProcessGroupStatusDescriptor.values()) {
					final String documentField = document.get(descriptor.getField());
					snapshot.addStatusMetric(descriptor.getDescriptor(), Long.parseLong(documentField));
				}
				return snapshot;
			});
		} catch (final IOException e) {
			throw new RuntimeException("Could not get status history for process group: " + processGroupId, e);
		}
	}

	@Override
	public StatusHistory getProcessorStatusHistory(final String processorId, final Date start, final Date end,
			final int preferredDataPoints, final boolean includeCounters) {
		try {
			return getStatusHistory(processorId, start, end, preferredDataPoints, (document) -> {
				final StandardStatusSnapshot snapshot = new StandardStatusSnapshot(DEFAULT_PROCESSOR_METRICS);
				snapshot.setTimestamp(new Date(Long.parseLong(document.get(TIMESTAMP))));

				for (ProcessorStatusDescriptor descriptor : ProcessorStatusDescriptor.values()) {
					final String documentField = document.get(descriptor.getField());
					snapshot.addStatusMetric(descriptor.getDescriptor(), Long.parseLong(documentField));
				}

				if (includeCounters) {
					// Find counter stats in the document and add them as extra metrics.
					StreamSupport.stream(document.spliterator(), false)
							.filter(indexableField -> indexableField.name().startsWith(COUNTER_PREFIX))
							.map(indexableField -> indexableField.name().substring(COUNTER_PREFIX.length()))
							.forEach(fieldName -> snapshot.addStatusMetric(
									new CounterMetricDescriptor<String>(fieldName, fieldName + " (5 mins)", fieldName,
											MetricDescriptor.Formatter.COUNT,
											s -> Long.parseLong(document.get(fieldName))),
									Long.parseLong(document.get(COUNTER_PREFIX + fieldName))));
				}
				
				return snapshot;
			});
		} catch (final IOException e) {
			throw new RuntimeException("Could not get status history for processor: " + processorId, e);
		}
	}

	@Override
	public StatusHistory getRemoteProcessGroupStatusHistory(final String remoteGroupId, final Date start,
			final Date end, final int preferredDataPoints) {
		try {
			return getStatusHistory(remoteGroupId, start, end, preferredDataPoints, (document) -> {
				final StandardStatusSnapshot snapshot = new StandardStatusSnapshot(DEFAULT_RPG_METRICS);
				snapshot.setTimestamp(new Date(Long.parseLong(document.get(TIMESTAMP))));

				for (RemoteProcessGroupStatusDescriptor descriptor : RemoteProcessGroupStatusDescriptor.values()) {
					final String documentField = document.get(descriptor.getField());
					snapshot.addStatusMetric(descriptor.getDescriptor(), Long.parseLong(documentField));
				}
				return snapshot;
			});
		} catch (final IOException e) {
			throw new RuntimeException("Could not get status history for remote process group: " + remoteGroupId, e);
		}
	}

	@Override
	public GarbageCollectionHistory getGarbageCollectionHistory(final Date start, final Date end) {
		try {
			// Get an index searcher for the up-to-date index
			final IndexSearcher indexSearcher = getIndexSearcher();

			// A null start should be considered to be the start of time.
			final long startLong = start != null ? start.getTime() : Long.MIN_VALUE;

			// A null end should be considered to be the current time.
			final long endLong = end != null ? end.getTime() : System.currentTimeMillis();

			// Find documents for the component ID within the specified time range, sorted
			// by timestamp.

			final BooleanQuery query = new BooleanQuery.Builder()
					.add(new BooleanClause(new TermQuery(new Term(DOCUMENT_TYPE, GC_SNAPSHOT_DOC)), Occur.MUST))
					.add(new BooleanClause(NumericDocValuesField.newSlowRangeQuery(TIMESTAMP, startLong, endLong),
							Occur.MUST))
					.build();
			final TopDocs topDocs = indexSearcher.search(query, Integer.MAX_VALUE,
					new Sort(new SortField(TIMESTAMP, Type.LONG)));

			final StandardGarbageCollectionHistory garbageCollectionHistory = new StandardGarbageCollectionHistory();

			if (topDocs.totalHits.value > 0) {
				// Turn each document into a snapshot
				final ListIterator<ScoreDoc> iterator = Arrays.asList(topDocs.scoreDocs).listIterator();
				Document document;
				do {
					final ScoreDoc scoreDoc = iterator.next();
					document = directoryReader.document(scoreDoc.doc);

					final String managerName = document.get(MEMORY_MANAGER_NAME);
					final Date timestamp = new Date(Long.parseLong(document.get(TIMESTAMP)));
					final long collectionCount = Long.parseLong(document.get(COLLECTION_COUNT));
					final long collectionMillis = Long.parseLong(document.get(COLLECTION_MILLIS));
					final GarbageCollectionStatus garbageCollectionStatus = new StandardGarbageCollectionStatus(
							managerName, timestamp, collectionCount, collectionMillis);
					garbageCollectionHistory.addGarbageCollectionStatus(garbageCollectionStatus);
				} while (iterator.hasNext());
			}

			return garbageCollectionHistory;
		} catch (final IOException e) {
			throw new RuntimeException("Could not get garbage collection history", e);
		}
	}

	public static Document createStatusSnapshotDocument(final ConnectionStatus connectionStatus, final Date timestamp) {
		final DocumentBuilder documentBuilder = new DocumentBuilder().storeString(DOCUMENT_TYPE, COMPONENT_SNAPSHOT_DOC)
				.storeLong(TIMESTAMP, timestamp.getTime()).storeString(COMPONENT_DETAIL_ID, connectionStatus.getId())
				.storeString(COMPONENT_DETAIL_GROUP_ID, connectionStatus.getGroupId())
				.storeString(COMPONENT_DETAIL_NAME, connectionStatus.getName())
				.storeString(COMPONENT_DETAIL_SOURCE_NAME, connectionStatus.getSourceName())
				.storeString(COMPONENT_DETAIL_DESTINATION_NAME, connectionStatus.getDestinationName());

		for (final ConnectionStatusDescriptor descriptor : ConnectionStatusDescriptor.values()) {
			documentBuilder.storeLong(descriptor.getField(),
					descriptor.getDescriptor().getValueFunction().getValue(connectionStatus));
		}

		return documentBuilder.build();
	}

	public static Document createStatusSnapshotDocument(final ProcessGroupStatus processGroupStatus,
			final Date timestamp) {
		final DocumentBuilder documentBuilder = new DocumentBuilder().storeString(DOCUMENT_TYPE, COMPONENT_SNAPSHOT_DOC)
				.storeLong(TIMESTAMP, timestamp.getTime()).storeString(COMPONENT_DETAIL_ID, processGroupStatus.getId())
				.storeString(COMPONENT_DETAIL_NAME, processGroupStatus.getName());

		for (final ProcessGroupStatusDescriptor descriptor : ProcessGroupStatusDescriptor.values()) {
			documentBuilder.storeLong(descriptor.getField(),
					descriptor.getDescriptor().getValueFunction().getValue(processGroupStatus));
		}

		return documentBuilder.build();
	}

	public static Document createStatusSnapshotDocument(final ProcessorStatus processorStatus, final Date timestamp) {
		final DocumentBuilder documentBuilder = new DocumentBuilder().storeString(DOCUMENT_TYPE, COMPONENT_SNAPSHOT_DOC)
				.storeLong(TIMESTAMP, timestamp.getTime()).storeString(COMPONENT_DETAIL_ID, processorStatus.getId())
				.storeString(COMPONENT_DETAIL_GROUP_ID, processorStatus.getGroupId())
				.storeString(COMPONENT_DETAIL_NAME, processorStatus.getName())
				.storeString(COMPONENT_DETAIL_TYPE, processorStatus.getType());

		for (final ProcessorStatusDescriptor descriptor : ProcessorStatusDescriptor.values()) {
			documentBuilder.storeLong(descriptor.getField(),
					descriptor.getDescriptor().getValueFunction().getValue(processorStatus));
		}

		final Map<String, Long> counters = processorStatus.getCounters();
		if (counters != null) {
			for (final Map.Entry<String, Long> entry : counters.entrySet()) {
				final String counterName = entry.getKey();
				final String fieldName = COUNTER_PREFIX + counterName;

				documentBuilder.storeLong(fieldName, processorStatus.getCounters().get(counterName));
			}
		}

		return documentBuilder.build();
	}

	public static Document createStatusSnapshotDocument(final RemoteProcessGroupStatus remoteProcessGroupStatus,
			final Date timestamp) {
		final DocumentBuilder documentBuilder = new DocumentBuilder().storeString(DOCUMENT_TYPE, COMPONENT_SNAPSHOT_DOC)
				.storeLong(TIMESTAMP, timestamp.getTime())
				.storeString(COMPONENT_DETAIL_ID, remoteProcessGroupStatus.getId())
				.storeString(COMPONENT_DETAIL_GROUP_ID, remoteProcessGroupStatus.getGroupId())
				.storeString(COMPONENT_DETAIL_NAME, remoteProcessGroupStatus.getName())
				.storeString(COMPONENT_DETAIL_URI, remoteProcessGroupStatus.getTargetUri());

		for (final RemoteProcessGroupStatusDescriptor descriptor : RemoteProcessGroupStatusDescriptor.values()) {
			documentBuilder.storeLong(descriptor.getField(),
					descriptor.getDescriptor().getValueFunction().getValue(remoteProcessGroupStatus));
		}

		return documentBuilder.build();
	}
}
